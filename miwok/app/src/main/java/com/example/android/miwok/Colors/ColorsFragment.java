package com.example.android.miwok.Colors;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.example.android.miwok.ListViewClickListener;
import com.example.android.miwok.R;
import com.example.android.miwok.Services.StaticDataProvider;
import com.example.android.miwok.WordAdapter;

public class ColorsFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.content_numbers, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        // Create the adapter to convert the array to views
        WordAdapter adapter = new WordAdapter(this.getContext(), new StaticDataProvider().GetColorsData(), "color");
        // Attach the adapter to a ListView
        ListView listView = (ListView) view.findViewById(R.id.numbersListView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new ListViewClickListener(getContext(), "color"));

    }
}
