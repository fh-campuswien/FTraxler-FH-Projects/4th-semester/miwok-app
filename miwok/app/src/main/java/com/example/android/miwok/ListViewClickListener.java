package com.example.android.miwok;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

public class ListViewClickListener implements AdapterView.OnItemClickListener
{
    private final Context _context;
    private final String _prefix;
    private final MediaPlayer _player;

    public ListViewClickListener(Context context, String prefix)
    {
        _context = context;
        _prefix = prefix;
        _player = new MediaPlayer();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        TextView nameView = view.findViewById(R.id.textView_english);
        String word = nameView.getText().toString().replace(' ', '_').replaceAll("[^a-zA-Z0-9_-]", "").toLowerCase();
        try
        {
            AssetFileDescriptor location = _context.getResources().openRawResourceFd(_context.getResources().getIdentifier(_prefix + "_" + word, "raw", _context.getPackageName()));
            _player.reset();
            _player.setDataSource(location.getFileDescriptor(), location.getStartOffset(), location.getDeclaredLength());
            _player.prepare();
            _player.start();
            location.close();
        }
        catch (Exception ex)
        {
        }
    }
}
