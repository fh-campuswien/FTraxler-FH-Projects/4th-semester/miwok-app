package com.example.android.miwok;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.example.android.miwok.Colors.ColorsFragment;
import com.example.android.miwok.FamilyMembers.FamilyMembersFragment;
import com.example.android.miwok.Numbers.NumbersFragment;
import com.example.android.miwok.Phrases.PhrasesFragment;

public class MainFragmentAdapter extends FragmentPagerAdapter
{
    private String _tabTitles[] = new String[]{"Numbers", "Colors", "Family Members", "Phrases"};
    private Context _context;

    public MainFragmentAdapter(FragmentManager fm, Context context)
    {
        super(fm);
        _context = context;
    }

    @Override
    public int getCount()
    {
        return _tabTitles.length;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            default:
            case 0:
                return new NumbersFragment();
            case 1:
                return new ColorsFragment();
            case 2:
                return new FamilyMembersFragment();
            case 3:
                return new PhrasesFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        // Generate title based on item position
        return _tabTitles[position];
    }
}
