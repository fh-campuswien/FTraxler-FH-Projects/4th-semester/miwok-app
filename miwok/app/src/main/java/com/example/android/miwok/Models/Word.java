package com.example.android.miwok.Models;

import android.content.Context;
import com.example.android.miwok.MainActivity;

public class Word
{
    private String _english;
    private String _miwok;

    public Word(String english, String miwok)
    {
        _english = english;
        _miwok = miwok;
    }

    public String getEnglish()
    {
        return _english;
    }

    public String getMiwok()
    {
        return _miwok;
    }
}
