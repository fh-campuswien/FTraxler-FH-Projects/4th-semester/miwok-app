package com.example.android.miwok.Services;

import com.example.android.miwok.Models.Word;

import java.util.List;

public interface IDataProvider
{
    List<Word> GetFamilyMemberData();

    List<Word> GetNumbersData();

    List<Word> GetColorsData();

    List<Word> GetPhrasesData();
}
