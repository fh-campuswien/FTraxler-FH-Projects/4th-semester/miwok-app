package com.example.android.miwok.Services;

import com.example.android.miwok.Models.Word;

import java.util.ArrayList;
import java.util.List;

public class StaticDataProvider implements IDataProvider
{
    @Override
    public List<Word> GetFamilyMemberData()
    {
        List<Word> list = new ArrayList<>();
        list.add(new Word("father", "әpә"));
        list.add(new Word("mother", "әṭa"));
        list.add(new Word("son", "angsi"));
        list.add(new Word("daughter", "tune"));
        list.add(new Word("older brother", "taachi"));
        list.add(new Word("younger brother", "chalitti"));
        list.add(new Word("older sister", "teṭe"));
        list.add(new Word("younger sister", "kolliti"));
        list.add(new Word("grandmother", "ama"));
        list.add(new Word("grandfather", "paapa"));
        return list;
    }

    @Override
    public List<Word> GetNumbersData()
    {
        List<Word> list = new ArrayList<>();
        list.add(new Word("one", "lutti"));
        list.add(new Word("two", "otiiko"));
        list.add(new Word("three", "tolookosu"));
        list.add(new Word("four", "oyyisa"));
        list.add(new Word("five", "massokka"));
        list.add(new Word("six", "temmokka"));
        list.add(new Word("seven", "kenekaku"));
        list.add(new Word("eight", "kawinta"));
        list.add(new Word("nine", "wo’e"));
        list.add(new Word("ten", "na’aacha"));
        return list;
    }

    @Override
    public List<Word> GetColorsData()
    {
        List<Word> list = new ArrayList<>();
        list.add(new Word("red", "weṭeṭṭi"));
        list.add(new Word("green", "chokokki"));
        list.add(new Word("brown", "ṭakaakki"));
        list.add(new Word("gray", "ṭopoppi"));
        list.add(new Word("black", "kululli"));
        list.add(new Word("white", "kelelli"));
        list.add(new Word("dusty yellow", "ṭopiisә"));
        list.add(new Word("mustard yellow", "chiwiiṭә"));
        return list;
    }

    @Override
    public List<Word> GetPhrasesData()
    {
        List<Word> list = new ArrayList<>();
        list.add(new Word("Where are you going?", "minto wuksus"));
        list.add(new Word("What is your name?", "tinnә oyaase'nә"));
        list.add(new Word("My name is...", "oyaaset..."));
        list.add(new Word("How are you feeling?", "michәksәs?"));
        list.add(new Word("I’m feeling good.", "kuchi achit"));
        list.add(new Word("Are you coming?", "әәnәs'aa?"));
        list.add(new Word("Yes, I’m coming.", "hәә’ әәnәm"));
        list.add(new Word("I’m coming.", "әәnәm"));
        list.add(new Word("Let’s go.", "yoowutis"));
        list.add(new Word("Come here.", "әnni'nem"));
        return list;
    }
}
