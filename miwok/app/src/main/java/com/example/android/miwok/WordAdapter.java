package com.example.android.miwok;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.android.miwok.Models.Word;

import java.util.List;

public class WordAdapter extends ArrayAdapter<Word>
{
    private final Context _context;
    private final List<Word> _wordList;
    private final String _prefix;

    public WordAdapter(@NonNull Context context, List<Word> list, String prefix)
    {
        super(context, 0, list);
        _context = context;
        _wordList = list;
        _prefix = prefix;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(_context).inflate(R.layout.numbers_list_item, parent, false);

        Word word = _wordList.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.textView_english);
        name.setText(word.getEnglish());

        TextView release = (TextView) listItem.findViewById(R.id.textView_miwok);
        release.setText(word.getMiwok());

        ImageView image = (ImageView) listItem.findViewById(R.id.textView_image);

        if (_prefix == null)
        {
            image.setVisibility(View.GONE);
        }
        else
        {
            try
            {
                image.setImageDrawable(_context.getResources().getDrawable(_context.getResources().getIdentifier(_prefix + "_" + word.getEnglish().replace(' ', '_'), "drawable", _context.getPackageName())));
            }
            catch (Exception ex)
            {
            }
        }

        return listItem;
    }
}
